
use std::fs::File;
use flate2::Compression;
use flate2::write::GzEncoder;
use flate2::read::GzDecoder;
use tar::Archive;

use std::collections::HashMap;
use std::io::stdin;
use std::{fs::*, process};
use text_io::read;
use uuid::Uuid;
use glob::glob;
use std::path::{Path, PathBuf};

const REPO_PATH: &str = "repo";
const FILES_TARGET: &str = "repo/files";
const BACKUP_PATH: &str = "repo/backup";

const HEADER: &str = "Добро пожаловать! На данный момент программа имеет следующий функционал:
        1. Создание файлов с uuid
        2. Удаление файлов
        3. Вывод файла (путь до него) по идентификатору
        4. Вывод всех файлов
        5. Изменение uuid файла (in progress)
        6. Создание резервной копии файлов
        7. Восстановление резервной копии";
        
const MAIN_ASK: &str = "Что вы хотите сделать? (введите цифру)
        1. Создать файл
        2. Удалить файл
        3. Вывести файл
        4. Вывести все файлы
        5. Изменить uuid (in progress)
        6. Создать резервную копию
        7. Восстановить резервную копию

        0. Помощь (шапка программы)
        255. Выход из программы";


fn main() {
    println!("{HEADER}"); // Вывод первичной информации
    repo_maker(); // Создание репозитория
    body_prog(); // Основной цикл программы 
}



struct FileService {
    storage: HashMap<Uuid, PathBuf>,
}

impl FileService {
    fn new() -> Self {
        Self {
            storage: HashMap::new(),
        }
    }

    fn create_file_with_uuid(&mut self, name: &str, ext: &str) -> Uuid {
        let uuid = Uuid::new_v4();
        let fname= format!("{name}-{uuid}.{ext}");
        let filename = fname.replace(&['/', '?', '|', '+', '#', '&', '%', '$',
        '^', '*', '@', '<', '>', '=', ';', ':'][..], "");

        let pather: PathBuf = [FILES_TARGET, &filename].iter().collect();
        let paz = pather.clone();
        File::create(pather).unwrap();
        self.storage.insert(uuid, paz);
        uuid
    }

    fn delete_file_by_uuid(&mut self, uuid: &str) {
        // if let Some(path_buf) = self.storage.remove(&uuid) {
        //     remove_file(path_buf).unwrap();
        // } else {
        //     println!("Файла с таким uuid не существует!")
        // }
        let uuid = uuid.trim();
        let delpath = &format!("{FILES_TARGET}/*{uuid}*")[..];
        // println!("{delpath}");
        for entry in glob(delpath).unwrap() {
            match entry {
            Ok(path) => {
                println!("Удаляю: {}", path.display());
                remove_file(path).unwrap();
                return;}
            Err(e) => {
                println!("{:?}", e);
                println!("Такого файла не сущесвует!");},
            }
        }
        println!("Такого файла не сущесвует!")
    }


    fn list_all_files(&self) {
        let entries = read_dir(&FILES_TARGET).unwrap();
        for entry in entries {
            let path = entry.unwrap().path();
            println!("{:?}", path);
        }
        print!("\n");
    }

    fn list_one_file(&self, uuid: &str) {
        let uuid = uuid.trim();
        let delpath = &format!("{FILES_TARGET}/*{uuid}*")[..];
        // println!("{delpath}");
        for entry in glob(delpath).unwrap() {
            match entry {
            Ok(path) => println!("{:?}", path.display()),
            Err(e) => {
                println!("{:?}", e);
                println!("Такого файла не сущесвует!");},
            }
        }
        // let path_buf = self.storage.get(&uuid);
        // path_buf.cloned()
    }

    // pub fn get_one_path(&mut self, uuid: &str) -> PathBuf {
    //     let uuid = uuid.trim();
    //     let delpath = &format!("{FILES_TARGET}/*{uuid}*")[..];
    //     // println!("{delpath}");
    //     for entry in glob(delpath).unwrap() {
    //         match entry {
    //         Ok(path) => return path,
    //         Err(e) => {
    //             println!("{:?}", e);
    //             println!("Такого файла не сущесвует!");
    //             },
    //         }
    //     }
    //     PathBuf::from("files/")
    // }

    // pub fn change_file_name(path: impl AsRef<Path>, uuid: &str) -> PathBuf {
    //     let path = path.as_ref();
    //     let mut result = path.to_owned();
    //     let filename = path.file_name();
    //     result.set_file_name(uuid);
    //     if let Some(ext) = path.extension() {
    //         result.set_extension(ext);
    //     }
    //     result
    // }

    // pub fn change_uuid(&mut self, uuid: &str, uuid2: &str) {
    //     let mypath = FileService::get_one_path(self, uuid);
    //     FileService::change_file_name(mypath, uuid);
        
    //     let delpath = &format!("{FILES_TARGET}/*{uuid}*")[..];
    //     // println!("{delpath}");
    //     for entry in glob(delpath).unwrap() {
    //         match entry {
    //         Ok(path) => {
                
    //         },
    //         Err(e) => {
    //             println!("{:?}", e);
    //             println!("Такого файла не сущесвует!");},
    //         }
    //     }
    // }
    
}

// Упрощающие функции
pub fn create_name() -> String {
    println!(r"WARNING! Запрещены такие символы как: / \ | + & ? # и подобные им!");
    print!("Введите название файла: ");
    let name: String = read!();
    name
}

pub fn create_ext() -> String {
    print!("Введите расширение файла без точки: ");
    let ext: String = read!();
    ext
}

pub fn enter_uuid() -> Uuid {
    let mut uuid: String = String::new();
    stdin().read_line(&mut uuid).expect("Не удалось обработать uuid.");
    let id: Uuid = Uuid::parse_str(&uuid).unwrap();
    id
}

pub fn enter_uuid_str() -> String {
    let mut uuid: String = String::new();
    stdin().read_line(&mut uuid).expect("Не удалось обработать uuid.");
    uuid
}

fn create_backup() -> Result<(), std::io::Error> {
        print!("Введите название архива без точки и расширения: ");
        let name: String = read!();
        print!("\n");
        let pathname: String = format!("repo/backup/{name}.tar.gz");
        let tar_gz = File::create(pathname)?;
        let enc = GzEncoder::new(tar_gz, Compression::default());
        let mut tar = tar::Builder::new(enc);
        tar.append_dir_all("repo/backup/", "repo/files/")?;
        Ok(())
}

fn cheack_create_backup() {
    let creator = create_backup();
    match creator {
        Ok(()) => (),
        Err(_) => println!("Не удалось создать архив!")
    }
}

fn extract_backup() -> Result<(), std::io::Error> {
    print!("Введите название архива: ");
    loop {
        let name: String = read!();
        let path = format!("repo/backup/{name}.tar.gz");
        let path = PathBuf::from(path);
        if !(path.is_file()) {
            println!("Архива с таким названием не существует!");
            continue;
        }
        let tar_gz = File::open(path)?;
        let tar = GzDecoder::new(tar_gz);
        let mut archive = Archive::new(tar);
        archive.unpack(FILES_TARGET)?;
        return Ok(())
    }
}

fn cheack_extract_backup() {
    let entries = read_dir(FILES_TARGET).unwrap();
    entries.for_each(|entry| {
        let file_path = entry.unwrap().path();
        if file_path.is_file() {
            remove_file(file_path).unwrap();
        }
    });
    
    let extractor = extract_backup();
    match extractor {
        Ok(()) => {(); println!("Резервная копия восстановлена.")}
        Err(_) => println!("Не удалось распаковать архив!")
    }    
}

pub fn mkdir_repo() {
    match create_dir(&REPO_PATH) {
        Err(why) => println!("!{:?}!", why.kind()),
        Ok(_) => {}
    }
    match create_dir(&FILES_TARGET) {
        Err(why) => println!("!{:?}!", why.kind()),
        Ok(_) => {}
    }
    match create_dir(&BACKUP_PATH) {
        Err(why) => println!("!{:?}!", why.kind()),
        Ok(_) => {}
    }
}


fn body_prog() {
    let mut file_service = FileService::new();
    
    loop {
        print!("{MAIN_ASK}\nОпция: ");

        let answer: String = read!();
        print!("\n");
        // stdin().read_line(&mut answer).expect("Не удалось обработать ответ");
        let answer: u8 = match answer.trim().parse() {
            Ok(num) => num,
            Err(_) => {println!("Введите цифру из списка!"); continue;}
        };

        match answer {
            1 => { //создать файл
                let name = &create_name();
                let ext = &create_ext();
                let showid = file_service.create_file_with_uuid(name, ext);
                println!("Ваш идентификатор: {showid}\n");
            },
            2 => { //удалить файл
                println!("Введите ваш uuid или его часть (не менее 4 символов): ");
                let uuid = enter_uuid_str();
                file_service.delete_file_by_uuid(&uuid);}
            3 => { // Вывести файл по uuid
                println!("Введите ваш uuid или его часть (не менее 4 символов): ");
                let uuid = enter_uuid_str();
                // let res = file_service.get_one_path(&uuid);
                // res.display()
                file_service.list_one_file(&uuid);}
            4 => file_service.list_all_files(), // Вывести все файлы
            // 5 => {
            //     print!("Введите ваш uuid: ");
            //     let uuid: String = read!();
            //     let uuid: &str = &uuid[..].trim();
                
            //     let uuid2: String = read!();
            //     let uuid2: &str = &uuid[..].trim();
            //     FileService::change_uuid(uuid, uuid2);}
            6 => cheack_create_backup(),
            7 => cheack_extract_backup(),
            0 => println!("{HEADER}\n"),
            255 => process::exit(0x0),
            _ => {
                println!("Введите цифру из списка!");
                continue;}
        }
    }
}


fn repo_maker() {
    println!("Хотите ли вы создать репозиторий для хранения файлов? 1/0");
    loop {
        let mut repo_ask: String = String::new();
        stdin().read_line(&mut repo_ask).expect("Введите 1 или 0!");

        let repo_ask: u8 = match repo_ask.trim().parse() {
            Ok(num) => num,
            Err(_) => {println!("Введите 1 или 0!"); continue}
        };

        match repo_ask {
            1 => {mkdir_repo(); println!("Репозиторий успешно создан."); return;}
            0 => {(); return;}
            _ => println!("Введите 1 или 0!"),
        }
    }
}
